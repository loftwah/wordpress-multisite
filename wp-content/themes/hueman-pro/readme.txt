# Hueman Pro v1.1.37
![Hueman - Pro](/screenshot.png)

> The premium version of the popular Hueman WordPress theme.

View more themes from this author: http://presscustomizr.com

## Demo and Documentation
* **Demo** : http://demo-hueman.presscustomizr.com/
* **Documentation** : http://docs.presscustomizr.com/article/236-first-steps-with-the-hueman-wordpress-theme


## Theme License
The **Hueman Pro WordPress theme** theme is licensed under GPLv3. See headers of files for further details.[GNU GPL v3.0 or later](http://www.gnu.org/licenses/gpl-3.0.en.html)


## Other Licenses
See headers of files for further details.

## Changelog
= 1.1.37 February 8th, 2020 =
* fixed : [html] element div not allowed as child of element button
* fixed : [html] Bad value for attribute datetime on element time
* fixed : adapt social links for Viber link type.
* fixed : style for .entry h2 span not compliant with accessibility standards
* fixed : featured image can be stretched when displayed in featured posts
* fixed : removed wrong href="#" on div elements in sharrre
* fixed : [pro header] html errors related to lazyloaded img in pro header
* added : new options to control the visibility of post metas ( date and categories ) in post lists

= 1.1.36 January 31st, 2020 =
* added : single post featured image => added new options, similar to the one of the page
* added : allow users to chose if featured image is cropped or not when displayed in a single post or page
* improved : implemented a CSS flexbox display for the search button

= 1.1.35 January 20th, 2020 =
* improved : [Header pro background] improved loading performances on mobiles
* added : sidebars => new options to customize the topbox default texts "Follow" and "More"

= 1.1.34 January 7th, 2020 =
* fixed : [Pro header] Slider element and slider js script have the same id attribute
* fixed : added noopener noreferrer relationship attributes to footer credit link
* added : an option allowing users to wrap the site title or logo in an H1 tag
* added : Flipboard icon to social icons
* added : [Pro header] let user set Hx tag globally and override this option on a per-slide basis

= 1.1.33 December 22nd, 2019 =
* fixed : old option favicon still printed, no way to remove it from the customizer
* fixed : social media links in the sidebar and footer should be using rel="noopener" or rel="noreferrer" when using target _blank
* fixed : search results as standard post list layout do not display page featured image
* fixed : sidebar icon toggles: namespace their CSS class name to avoid conflicting plugins issues
* fixed : custom widget zones: a static front page shows widget zones assigned to "Pages"
* fixed : The mobile menu doesn't automatically close when clicking on an internal anchor link item
* fixed : searchform input not compliant with latest accessibility standards + generating problems with cache plugins
* fixed : sidebar width in js code => localize width values instead of using hardcoded values
