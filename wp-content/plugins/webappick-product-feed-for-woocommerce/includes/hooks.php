<?php
/**
 * Default Hooks
 * @package WooFeed
 * @subpackage WooFeed_Helper_Functions
 * @version 1.0.0
 * @since WooFeed 3.3.0
 * @author KD <mhamudul.hk@gmail.com>
 * @copyright WebAppick
 */

if ( ! defined( 'ABSPATH' ) ) {
	die(); // Silence...
}
/** @define "WOO_FEED_ADMIN_PATH" "./../admin/" */ // phpcs:ignore

add_action( 'woo_feed_before_product_loop', 'woo_feed_apply_hooks_before_product_loop', 10, 1 );
add_action( 'woo_feed_after_product_loop', 'woo_feed_remove_hooks_before_product_loop', 10, 1 );
// End of file hooks.php.