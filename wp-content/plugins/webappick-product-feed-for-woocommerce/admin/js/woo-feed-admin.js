// noinspection JSUnresolvedVariable,ES6ConvertVarToLetConst,SpellCheckingInspection
(function ($, window, document, wpAjax, opts) {
    "use strict";
    /**
     * All of the code for your admin-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     */

    /**
     * disable element utility
     * @since 3.1.9
     */
    $.fn.disabled = function( status ) {
        $(this).each( function(){
            // noinspection ES6ConvertVarToLetConst
            var self = $(this), prop = 'disabled';
            if( typeof( self.prop( prop ) ) !== 'undefined' ) {
                self.prop( prop, status === void 0 || status === true  );
            } else {
                ! 0 === status ? self.addClass( prop ) : self.removeClass( prop );
            }
        } );
        return self; // method chaining
    };
    $.fn.isDisabled = function() {
        // noinspection ES6ConvertVarToLetConst
        var self = $(this), prop = 'disabled';
        return ( typeof( self.prop( prop ) ) !== 'undefined' ) ? self.prop( prop ) : self.hasClass( prop );
    };

    function clearTooltip( event ) {
        // noinspection SpellCheckingInspection
        $(event.currentTarget).removeClass( function (index, className) {
            return (className.match (/\btooltipped-\S+/g) || []).join(' ');
        } ).removeClass('tooltipped').removeAttr('aria-label');
    }

    function showTooltip( elem, msg ) {
        // noinspection SpellCheckingInspection
        $( elem ).addClass('tooltipped tooltipped-s').attr( 'aria-label', msg );
    }

    function fallbackMessage(action) {
        // noinspection ES6ConvertVarToLetConst
        var actionMsg = '', actionKey = (action === 'cut' ? 'X' : 'C');
        if (/iPhone|iPad/i.test(navigator.userAgent)) {
            actionMsg = 'No support :(';
        } else if (/Mac/i.test(navigator.userAgent)) {
            actionMsg = 'Press ⌘-' + actionKey + ' to ' + action;
        } else {
            actionMsg = 'Press Ctrl-' + actionKey + ' to ' + action;
        }
        return actionMsg;
    }

    /* global ajaxurl, wpAjax, postboxes, pagenow, alert, deleteUserSetting, typenow, adminpage, thousandsSeparator, decimalPoint, isRtl */
    $(window).load(function () {
        // noinspection ES6ConvertVarToLetConst,SpellCheckingInspection
        var $copyBtn = $('.toClipboard'), clipboard,
            googleCategories,
            helper = {
                in_array: function( needle, haystack ) {
                    try {
                        return haystack.indexOf( needle ) !== -1;
                    } catch( e ) {
                        return false;
                    }
                },
                selectize_render_item: function( data, escape ) {
                    return '<div class="item wapk-selectize-item">' + escape(data.text) + '</div>'; // phpcs:ignore WordPressVIPMinimum.JS.StringConcat.Found
                },
                ajax_fail: function ( e ) {
                    console.log( e );
                    alert( ( e.hasOwnProperty( 'statusText' ) && e.hasOwnProperty( 'status' ) ) ? opts.ajax.error + '\n' + e.statusText + ' (' + e.status + ')' : e );
                }
            }, // helper functions
            feedEditor = {
                init: function() {
                    // noinspection ES6ConvertVarToLetConst
                    var outOfStockVisibilityRow = $('.out-of-stock-visibility');
                    // Initialize Table Sorting
                    // noinspection SpellCheckingInspection,JSUnresolvedFunction
                    $('.sorted_table').sortablesd({
                        containerSelector: 'table',
                        itemPath: '> tbody',
                        itemSelector: 'tr',
                        handle: 'i.wf_sortedtable',
                        placeholder: '<tr class="placeholder"><td colspan="9"></td></tr>',
                    });
                    $('.selectize').each(function(){
                        // noinspection ES6ConvertVarToLetConst
                        var self = $(this), plugins = self.data('plugins');
                        self.selectize({
                            plugins: plugins ? plugins.split(',') : [],//['remove_button'],
                            render: { item: helper.selectize_render_item, }
                        })
                    });
                    $('[name="is_outOfStock"], [name="product_visibility"]').on( 'change', function(){
                        if( $('[name="is_outOfStock"]:checked').val() === 'n' && $('[name="product_visibility"]:checked').val() === '1' ) {
                            outOfStockVisibilityRow.show();
                        } else {
                            outOfStockVisibilityRow.hide();
                        }
                    } ).trigger( 'change' );
                    // Attribute type selection
                    $(document).on('change', '.attr_type', function () {
                        // noinspection ES6ConvertVarToLetConst
                        var type = $(this).val(), row = $(this).closest('tr');
                        if (type === 'pattern') {
                            row.find('.wf_attr').hide();
                            row.find('.wf_attr').val('');
                            row.find('.wf_default').show();
                        } else {
                            row.find('.wf_attr').show();
                            row.find('.wf_default').hide();
                            row.find('.wf_default').val('');
                        }
                    });
                    $(document).on( 'change', '.wf_mattributes, .attr_type', function() {
                        // noinspection ES6ConvertVarToLetConst
                        var row = $(this).closest( 'tr' ),
                            attribute = row.find( '.wf_mattributes' ),
                            type = row.find('.attr_type'),
                            valueColumn = row.find('td:eq(4)'),
                            provider = $('#provider').val();
                        if( attribute.val() === 'current_category' && type.val() === 'pattern' && helper.in_array( provider, [ 'google', 'facebook', 'pinterest' ] ) ) {
                            if( valueColumn.find('select.selectize').length === 0 ) {
                                // noinspection ES6ConvertVarToLetConst
                                var selectizeOpts = {
                                        options: googleCategories,
                                        config: { render: { item: helper.selectize_render_item, } },
                                    },
                                    select = valueColumn.find('.wf_attributes select');
                                valueColumn.find('input.wf_default').remove();
                                valueColumn.append('<span class="wf_default wf_attributes"><select name="default[]" class="selectize"></select></span>');
                                valueColumn.find('.wf_attributes select').selectize({render: {item: helper.selectize_render_item,}});
                                valueColumn.append('<span style="font-size:x-small;"><a style="color: red" href="http://webappick.helpscoutdocs.com/article/19-how-to-map-store-category-with-merchant-category" target="_blank">Learn More..</a></span>');
                                valueColumn.append('<span class="spinner is-active" style="margin: 0;"></span>');
                                if( ! googleCategories ) {
                                    wpAjax.send( 'get_google_categories', {
                                        type: 'GET',
                                        data: { _ajax_nonce: opts.nonce, action: "get_google_categories", provider: provider }
                                    } ).then( function( r ) {
                                        googleCategories = r;
                                        selectizeOpts.options = r;
                                        feedEditor.renderSelectize( select, selectizeOpts );
                                        valueColumn.find('.spinner').remove();
                                    } ).fail( helper.ajax_fail );
                                } else {
                                    feedEditor.renderSelectize( select, selectizeOpts );
                                    valueColumn.find('.spinner').remove();
                                }
                            }
                        } else {
                            if( attribute.val() !== 'current_category' && valueColumn.find('input.wf_default').length === 0 ) {
                                valueColumn.find('span').remove();
                                valueColumn.append( '<input autocomplete="off" class="wf_default wf_attributes"  type="text" name="default[]" value="">' );
                                if( type.val() !== 'pattern' ) {
                                    valueColumn.find('input.wf_default').hide();
                                }
                            }
                        }
                    } );
                },
                renderSelectize: function( el, config ) {
                    // noinspection ES6ConvertVarToLetConst
                    var c = $.extend({}, {
                        options: {},
                        settings: {render: {item: helper.selectize_render_item,}}
                    }, config);
                    el.selectize(c.settings);
                    // noinspection ES6ConvertVarToLetConst
                    var s = el[0].selectize;
                    s.clearOptions();
                    if( Object.keys(c.options).length ) {
                        // noinspection ES6ConvertVarToLetConst
                        for( var i in c.options ) {
                            if( c.options.hasOwnProperty( i ) ) {
                                s.addOption( c.options[i] );
                            }
                        }
                    }
                    s.refreshOptions( true );
                    return el;
                },
                renderMerchantInfo: function( merchantInfo, feedType, r ) {
                    // noinspection ES6ConvertVarToLetConst
                    for( var k in r ) {
                        if( r.hasOwnProperty( k ) ) {
                            merchantInfo.find( '.merchant-info-section.' + k + ' .data' ).html( r[k] ); // phpcs:ignore WordPressVIPMinimum.JS.HTMLExecutingFunctions.html
                            if( k === 'feed_file_type' ) {
                                // noinspection ES6ConvertVarToLetConst,JSUnresolvedVariable
                                var types = r[k].split(",").map(function(t){return t.trim().toLowerCase()}).filter(function(t){
                                    // noinspection JSUnresolvedVariable
                                    return t !== '' && t !== opts.na.toLowerCase()
                                });
                                if( types.length ) {
                                    feedType.find('option').removeAttr('selected').each( function(){
                                        // noinspection ES6ConvertVarToLetConst
                                        var opt = $(this);
                                        opt.val() && ! helper.in_array(opt.val(),types) ? opt.disabled( ! 0) : opt.disabled( ! 1);
                                    } );
                                    if( types.length === 1 ) feedType.find('option[value="' + types[0] + '"]').attr( 'selected', 'selected' );
                                } else feedType.find('option').disabled( ! 1 );
                            }
                        }
                    }
                    merchantInfo.find( '.spinner' ).removeClass( 'is-active' );
                    feedType.disabled( ! 1 );
                    feedType.trigger('change');
                    feedType.parent().find('.spinner').removeClass( 'is-active' );
                },
                renderMerchantTemplate: function( feedForm, r ) {
                    // insert server response
                    feedForm.html( r ); // phpcs:ignore WordPressVIPMinimum.JS.HTMLExecutingFunctions.html
                    feedEditor.init();
                }
            }, // Feed Editor Table
            merchantInfoCache = [],
            merchantTemplateCache = [];


        $('[data-toggle_slide]').on('click', function(e) {
            e.preventDefault();
            $($(this).data('toggle_slide')).slideToggle('fast');
        });
        postboxes.add_postbox_toggles(pagenow);
        if( ! ClipboardJS.isSupported() || /iPhone|iPad/i.test(navigator.userAgent) ) {
            $copyBtn.find('img').hide(0);
        } else {
            $copyBtn.each( function(){
                $(this).on( 'mouseleave', clearTooltip ).on( 'blur', clearTooltip );
            } );
            clipboard = new ClipboardJS('.toClipboard');
            clipboard.on( 'error', function( event ) { showTooltip( event.trigger, fallbackMessage( event.action ) ) } )
                .on( 'success', function( event ) { showTooltip( event.trigger, 'Copied!' ) } );
        }
        // initialize editor
        feedEditor.init();
        // Template loading ui conflict
        if( $(location). attr("href").match( /webappick.*feed/g ) != null ) {
            $('#wpbody-content').addClass('woofeed-body-content');
        }

        // Generate Feed Add Table Row
        $(document).on('click', '#wf_newRow', function () {
            $("#table-1 tbody tr:first").clone().find('input').val('').end().find("select:not('.wfnoempty')").val('').end().insertAfter("#table-1 tbody tr:last");
            $('.outputType').each(function (index) {
                //do stuff to each individually.
                $(this).attr('name', "output_type[" + index + "][]"); //sets the val to the index of the element, which, you know, is useless
            });
        });

        // XML Feed Wrapper
        $('#feedType,#provider').on('change', function () {
            // noinspection ES6ConvertVarToLetConst,SpellCheckingInspection
            var type = $('#feedType').val(), provider = $('#provider').val(), itemWrapper = $('.itemWrapper'), wf_csvtxt = $('.wf_csvtxt');
            // noinspection JSUnresolvedVariable
            if ( type === 'xml' ) {
                itemWrapper.show(); wf_csvtxt.hide();
            } else if ( type === 'csv' || type === 'txt' ) {
                itemWrapper.hide(); wf_csvtxt.show();
            } else if ( type === '' ) {
                itemWrapper.hide(); wf_csvtxt.hide();
            }
            if( type !== "" && helper.in_array( provider, ['google', 'facebook'] ) ) itemWrapper.hide();
            // noinspection SpellCheckingInspection
            if( provider === 'criteo' ) {
                itemWrapper.find('input[name="itemsWrapper"]').val("channel");
                itemWrapper.find('input[name="itemWrapper"]').val("item");
            }
        }).trigger( 'change' );
        // Tooltip only Text
        {
            // noinspection SpellCheckingInspection
            $('.wfmasterTooltip').hover(function () {
                // Hover over code
                // noinspection ES6ConvertVarToLetConst,SpellCheckingInspection
                var title = $(this).attr('wftitle');
                // noinspection SpellCheckingInspection
                $(this).data('tipText', title).removeAttr('wftitle');
                // noinspection SpellCheckingInspection
                $('<p class="wftooltip"></p>').text(title).appendTo('body').fadeIn('slow');
            }, function () {
                // Hover out code
                // noinspection SpellCheckingInspection
                $(this).attr('wftitle', $(this).data('tipText'));
                // noinspection SpellCheckingInspection
                $('.wftooltip').remove();
            }).mousemove(function (e) {
                // noinspection ES6ConvertVarToLetConst,SpellCheckingInspection
                var mousex = e.pageX + 20, //Get X coordinates
                    mousey = e.pageY + 10; //Get Y coordinates
                // noinspection SpellCheckingInspection
                $('.wftooltip').css({top: mousey, left: mousex})
            });
        }

        // Add New Condition for Filter
        $(document).on('click', '#wf_newFilter', function () {
            $("#table-advanced-filter tbody tr:eq(0)").show().clone().find('input').val('').end().find('select').val('').end().insertAfter("#table-advanced-filter tbody tr:last");
            // noinspection SpellCheckingInspection
            $(".fsrow:gt(2)").prop('disabled', false);
            $(".daRow:eq(0)").hide();
        });

        // Attribute type selection for dynamic attribute
        $(document).on('change', '.dType', function () {
            // noinspection ES6ConvertVarToLetConst
            var type = $(this).val(), row = $(this).closest('tr');
            if (type === 'pattern') {
                row.find('.value_attribute').hide();
                row.find('.value_pattern').show();
            } else if (type === 'attribute') {
                row.find('.value_attribute').show();
                row.find('.value_pattern').hide();
            } else if (type === 'remove') {
                row.find('.value_attribute').hide();
                row.find('.value_pattern').hide();
            }
        });

        // Generate Feed Table Row Delete
        $(document).on('click', '.delRow', function () {
            $(this).closest('tr').remove();
        });

        //Expand output type
        $(document).on('click', '.expandType', function () {
            // noinspection ES6ConvertVarToLetConst
            var row = $(this).closest('tr');
            $('.outputType').each(function (index) {
                //do stuff to each individually.
                $(this).attr('name', "output_type[" + index + "][]");
            });
            row.find('.outputType').attr('multiple', 'multiple');
            row.find('.contractType').show();
            $(this).hide();
        });

        //Contract output type
        $(document).on('click', '.contractType', function () {
            // noinspection ES6ConvertVarToLetConst
            var row = $(this).closest('tr');
            $('.outputType').each(function (index) {
                //do stuff to each individually.
                $(this).attr('name', "output_type[" + index + "][]");
            });
            row.find('.outputType').removeAttr('multiple');
            row.find('.expandType').show();
            $(this).hide();
        });

        // Generate Feed Form Submit
        $(".generateFeed").validate();
        $(document).on('submit', '#generateFeed', function () {
            //event.preventDefault();
            // Feed Generating form validation
            $(this).validate();
            if ($(this).valid()) {
                // noinspection SpellCheckingInspection
                $(".makeFeedResponse").show().html( '<b style="color: darkblue;"><i class="dashicons dashicons-sos wpf_spin"></i> ' + opts.form.generate + '</b>' ); // phpcs:ignore WordPressVIPMinimum.JS.HTMLExecutingFunctions.html, WordPressVIPMinimum.JS.StringConcat.Found
            }
        });

        // Generate Update Feed Form Submit
        // noinspection SpellCheckingInspection
        $(".updatefeed").validate();
        $('[name="save_feed_config"]').on( 'click', function( e ) {
            e.preventDefault();
            $('#updatefeed').trigger( 'submit', { save: true } );
        } );
        $(document).on('submit', '#updatefeed', function ( e, data ) {
            // Feed Generating form validation
            $(this).validate();
            if ( $(this).valid() ) {
                // noinspection SpellCheckingInspection
                $(".makeFeedResponse").show().html( '<b style="color: darkblue;"><i class="dashicons dashicons-sos wpf_spin"></i> ' + ( data && data.save ? opts.form.save : opts.form.generate ) + '</b>' ); // phpcs:ignore WordPressVIPMinimum.JS.HTMLExecutingFunctions.html, WordPressVIPMinimum.JS.StringConcat.Found
            }
        });

        // Get Merchant View
        $("#provider").on('change', function ( event ) {
            event.preventDefault();
            if( ! $(this).closest('.generateFeed').hasClass('add-new') ) return; // only for new feed.
            // noinspection ES6ConvertVarToLetConst
            var merchant = $(this).val(),
                feedType = $("#feedType"),
                feedForm = $("#providerPage"),
                merchantInfo = $('#feed_merchant_info');
            // set loading..
            // noinspection JSUnresolvedVariable
            feedForm.html('<h3><span style="float:none;margin: -3px 0 0;" class="spinner is-active"></span> ' + opts.form.loading_tmpl + '</h3>'); // phpcs:ignore WordPressVIPMinimum.JS.HTMLExecutingFunctions.html, WordPressVIPMinimum.JS.StringConcat.Found
            merchantInfo.find( '.spinner' ).addClass( 'is-active' );
            feedType.disabled( ! 0 ); // disable dropdown
            feedType.parent().find('.spinner').addClass( 'is-active' );
            merchantInfo.find( '.merchant-info-section .data' ).html(''); // remove previous data
            // Get Merchant info for selected Provider/Merchant
            if( merchantInfoCache.hasOwnProperty( merchant ) ) {
                feedEditor.renderMerchantInfo( merchantInfo, feedType, merchantInfoCache[merchant] );
            } else {
                wpAjax.send( 'woo_feed_get_merchant_info', {
                    type: 'GET',
                    data: { nonce: opts.nonce, action: "get_feed_merchant", provider: merchant }
                } ).then( function( r ) {
                    merchantInfoCache[merchant] = r;
                    feedEditor.renderMerchantInfo( merchantInfo, feedType, r );
                } ).fail( helper.ajax_fail );
            }

            // Get FeedForm For Selected Provider/Merchant
            if( merchantTemplateCache.hasOwnProperty( merchant ) ) {
                feedEditor.renderMerchantTemplate( feedForm, merchantTemplateCache[merchant] );
            } else {
                wpAjax.post( 'get_feed_merchant', {
                    _ajax_nonce: opts.nonce, action: "get_feed_merchant", merchant: merchant
                }, )
                    .then( function( r ) {
                        merchantTemplateCache[merchant] = r;
                        feedEditor.renderMerchantTemplate( feedForm, r );
                    } ).fail( helper.ajax_fail );
            }
        });

        // Feed Active and Inactive status change via ajax
        $('.woo_feed_status_input').on('change',function(){
            // noinspection ES6ConvertVarToLetConst
            var  $feedName = $(this).val(), counter = ( $(this)[0].checked ) ? 1 : 0;
            wpAjax.post( 'update_feed_status', { _ajax_nonce: opts.nonce, feedName: $feedName, status: counter }, );
        });

        // Adding for Copy-to-Clipboard functionality in the settings page
        $("#woo_feed_settings_error_copy_clipboard_button").on('click', function() {
            $('#woo_feed_settings_error_report').select();
            document.execCommand('copy');
            if (window.getSelection) {window.getSelection().removeAllRanges();}
            else if (document.selection) {document.selection.empty();}
        });

        //Checking whether php ssh2 extension is added or not
        // noinspection SpellCheckingInspection
        $(document).on('change', '.ftporsftp', function () {
            // noinspection ES6ConvertVarToLetConst
            var server = $(this).val(), status = $('.ssh2_status');
            if (server === 'sftp') {
                // noinspection JSUnresolvedVariable
                status.show().css('color', 'dodgerblue').text(opts.form.sftp_checking);
                // noinspection JSUnresolvedVariable
                wpAjax.post('get_ssh2_status', {_ajax_nonce: opts.nonce, server: server})
                    .then(function (response) {
                        if ( response === 'exists' ) {
                            // noinspection JSUnresolvedVariable
                            status.css('color', '#2CC185').text(opts.form.sftp_available);
                            setTimeout( function () {
                                status.hide();
                            }, 1500 );
                        } else {
                            // noinspection JSUnresolvedVariable
                            status.show().css('color', 'red').text(opts.form.sftp_warning);
                        }
                    })
                    .fail( function( e ) {
                        status.hide();
                        helper.ajax_fail( e );
                    } );
            } else {
                status.hide();
            }
        });
    });
}( jQuery, window, document, wp.ajax, wpf_ajax_obj ));