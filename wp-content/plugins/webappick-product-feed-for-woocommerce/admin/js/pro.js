// noinspection JSUnresolvedVariable,ES6ConvertVarToLetConst,SpellCheckingInspection
(function ($, window, document, wpAjax, opts) {
    /* global ajaxurl, wpAjax, postboxes, pagenow, alert, deleteUserSetting, typenow, adminpage, thousandsSeparator, decimalPoint, isRtl */
    $(window).load(function () {
        // noinspection ES6ConvertVarToLetConst,SpellCheckingInspection
        var sliders = $('.wapk-slider');
        if( sliders.length ) {
            sliders.slick({
                autoplay: true,
                dots: true,
                centerMode: true,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                lazyLoad: 'progressive'
            });
        }
    } );
}( jQuery, window, document, wp.ajax, wpf_ajax_obj ));